//
//  PrimedExample.swift
//  PrimedIO_Example
//
//  Created by Bosko Petreski on 10/19/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import PrimedSwift

class PrimedExample: UIViewController {

    @IBOutlet var txtContsole : UITextView!
    
    @IBAction func onBtnConvert(){
        let dictData : [String:Any] = ["device":"iphone","userid":"someuserid"]
        Primed.shared.convert(ruuid: "RUUID_GO_HERE", data: dictData)
    }
    @IBAction func onBtnPersonalise(){
        let dictSignals : [String:String] = ["device":"iphone","userid":"someuserid", "browserName": "000000"]

        Primed.shared.personalise(campaign: "frontpage.recommendations", signals: dictSignals, limit: 3, abvariantLabel: "A", success: { (response) in
            //Handle response
            let output : NSMutableString = ""
            output.append("guuid:" + response.guuid)
            for result in response.results{
                output.append("ruuid: " + result.ruuid)
                output.append("value: \(result.value)")
            }
            self.txtContsole.text = String(output)
        }) { (message) in
            //Handle message
            print(message)
        }
    }
    @IBAction func onBtnCheckHealth(){
        Primed.shared.health(success: { (message) in
            //Handle message
            print(message)
        }) { (message) in
            //Handle message
            print(message)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PrimedTracker.sharedTracker.customProperties = ["omroep": "noord", "domein": "dev"]
        
        PrimedTracker.sharedTracker.initPrimedTracker(publicKey: "pubkey", secretKey: "secretkey", connectionString: ProcessInfo.processInfo.environment["DELIVERY_SERVER_URI"]!, trackingConnectionString: ProcessInfo.processInfo.environment["COLLECTOR_URI"]!)

        Primed.shared.initPrimed(publicKey: "pubkey", secretKey: "secretkey", connectionString: ProcessInfo.processInfo.environment["DELIVERY_SERVER_URI"]!)
    }

}
