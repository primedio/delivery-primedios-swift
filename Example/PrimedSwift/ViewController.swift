//
//  ViewController.swift
//  PrimedIO
//
//  Created by PrimedIO on 10/18/2018.
//  Copyright (c) 2018 PrimedIO. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func onBtnOpenPrimed(){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "Primed")
        self.present(controller!, animated: true, completion: nil)
    }
    @IBAction func onBtnOpenPrimedTracker(){
        let controller = self.storyboard?.instantiateViewController(withIdentifier: "PrimedTracker")
        self.present(controller!, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

