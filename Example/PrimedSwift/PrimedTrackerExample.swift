//
//  PrimedTrackerExample.swift
//  PrimedIO_Example
//
//  Created by Bosko Petreski on 10/19/18.
//  Copyright © 2018 CocoaPods. All rights reserved.
//

import UIKit
import PrimedSwift

class PrimedTrackerExample: UIViewController,UIScrollViewDelegate {

    var lastContentOffset : Float = 0
    
    @IBOutlet var txtContsole : UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        PrimedTracker.sharedTracker.customProperties = ["omroep": "noord", "domein": "dev"]
        
        PrimedTracker.sharedTracker.initPrimedTracker(publicKey: "pubkey", secretKey: "secretkey", connectionString: ProcessInfo.processInfo.environment["DELIVERY_SERVER_URI"]!, trackingConnectionString: ProcessInfo.processInfo.environment["COLLECTOR_URI"]!)
        
        

        
        if #available(iOS 10.0, *) {
            Timer.scheduledTimer(withTimeInterval: 10, repeats: true) { (timer) in
                self.trackLocation()
            }
        } else {
            Timer.scheduledTimer(timeInterval: 10, target: self, selector:#selector(self.trackLocation) , userInfo: nil, repeats: true)
        }

    }
    
    @objc func trackLocation(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        PrimedTracker.sharedTracker.trackPositionChange(location: appDelegate.locationManager.location!)
    }
    
    @IBAction func onBtnTrackView(){
        PrimedTracker.sharedTracker.trackView(uri: "Test View Example", customProperties: ["latitude": 1.1, "longitude": 1.1, "accuracy": 5.1])
    }

    @IBAction func onBtnTrackCustomEvent(){
        PrimedTracker.sharedTracker.trackCustomEvent(eventType: "CustomEvent", customProperties: ["uri": "uri", "itemId": "itemId", "fraction": 90.001])
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = Float(scrollView.contentOffset.x)
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if (self.lastContentOffset < Float(scrollView.contentOffset.y)) {
            let distance : Int = Int(Float(scrollView.contentOffset.y) - self.lastContentOffset)
            PrimedTracker.sharedTracker.trackScroll(scrollDirection: .ScrollDirection_DOWN, distance: distance)
        }
        else {
            let distance : Int = Int(self.lastContentOffset - Float(scrollView.contentOffset.y))
            PrimedTracker.sharedTracker.trackScroll(scrollDirection: .ScrollDirection_UP, distance: distance)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch = touches.first!
        let location = touch.location(in: self.view)
        
        PrimedTracker.sharedTracker.trackClick(x: Int(location.x), y: Int(location.y))
        
    }

}
