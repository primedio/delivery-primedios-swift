#
# Be sure to run `pod lib lint PrimedIO.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'PrimedSwift'
    s.version          = '0.0.3'
    s.summary          = 'IOS SDK for retrieving predictions from the Primed Backend, as well as track behavior'
    s.description      = <<-DESC
    IOS SDK for retrieving predictions from the Primed Backend, as well as track behavior.
    DESC
    
    s.homepage         = 'https://gitlab.com/primedio/delivery-primedios-swift'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'PrimedIO' => 'info@primed.io' }
    s.source           = { :git => 'https://gitlab.com/primedio/delivery-primedios-swift.git', :tag => s.version.to_s }
    s.ios.deployment_target = '9.0'
    s.source_files = 'PrimedSwift/Classes/**/*.swift'
    s.swift_version = '4.2'
    s.framework = 'SystemConfiguration' , 'UIKit' , 'CoreLocation'
    s.dependency  'Socket.IO-Client-Swift'
    s.dependency 'CryptoSwift'
    s.dependency 'Alamofire'
end
