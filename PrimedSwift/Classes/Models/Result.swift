//
//  Result.swift
//  PrimedIO
//
//  Created by PrimedIO on 11/23/18.
//

import UIKit

public class Result: NSObject {
    public var ruuid: String = ""
    public var value: Dictionary<String, Any> = [:]
    
    public init(ruuid: String , value : Dictionary<String ,Any>){
        self.ruuid = ruuid
        self.value = value
    }
}
