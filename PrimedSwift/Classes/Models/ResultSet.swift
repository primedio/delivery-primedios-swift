//
//  ResultSet.swift
//  PrimedIO
//
//  Created by PrimedIO on 11/23/18.
//

import UIKit

public class ResultSet: NSObject {
    public var guuid: String = ""
    public var results: [Result] = []
    
    public init(guuid: String , results : [Result]){
        self.guuid = guuid
        self.results = results
    }
}
