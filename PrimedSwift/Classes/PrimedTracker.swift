//
//  PrimedTracker.swift
//  Pods-PrimedIO_Example
//
//  Created by PrimedIO on 8/29/18.
//

import UIKit
import SocketIO
import CoreLocation
import SystemConfiguration

public class PrimedTracker : Primed {
    
    public enum InteractionType : String{
        case InteractionType_LEFT = "LEFT"
        case InteractionType_RIGHT = "RIGHT"
        case InteractionType_MIDDLE = "MIDDLE"
        case InteractionType_OTHER = "OTHER"
        case InteractionType_LONGPRESS = "LONGPRESS"
        case InteractionType_NONE = "NONE"
    }
    
    public enum ScrollDirection : String{
        case ScrollDirection_UP = "UP"
        case ScrollDirection_DOWN = "DOWN"
        case ScrollDirection_LEFT = "LEFT"
        case InteractionType_OTHER = "OTHER"
        case ScrollDirection_RIGHT = "RIGHT"
    }

    public static let sharedTracker = PrimedTracker()
    
    var heartBeatIncrement: Int = 0
    var sid: String = NSUUID().uuidString
    var did: String = (UIDevice.current.identifierForVendor?.uuidString)!
    public var customProperties: Dictionary<String,Any>? = nil
    
    var savedPayloads: NSMutableArray
    var savedPayloadsBufferSize = 100
    
    var heartBeatTimer:Timer?
    var payloadQueueTimer:Timer?
    
    var socket: SocketIOClient!
    var primedTrackerAvailable: Bool = false
    var manager: SocketManager!
    var socketConnected: Bool = false
    
    override init(){
        self.savedPayloads = NSMutableArray.init(contentsOfFile: PrimedTracker.pathPayloads()) ?? NSMutableArray();
    }
    
    //MARK: CUSTOM FUNCTIONS
    static func pathPayloads() -> String! {
        let documentsPathString = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
        let path = String(documentsPathString! + "/primed_event_queue.plist")
        return path
    }
    
    func saveToPlist(payload: Dictionary<String,Any>){
        self.savedPayloads.add(payload)
        self.savedPayloads.write(toFile: PrimedTracker.pathPayloads(), atomically: true)
    }
    
    func removeFirstInPayloads(){
        self.savedPayloads.removeObject(at: 0)
        self.savedPayloads.write(toFile: PrimedTracker.pathPayloads(), atomically: true)
    }
    
    func getUA() -> String{
        return String(UIDevice.current.model + ";" + UIDevice.current.systemVersion)
    }
    
    static func getDate_iso8601() -> String{
        let dateFormatter = DateFormatter()
        let enUSPosixLocale = Locale(identifier: "en_US_POSIX")
        dateFormatter.locale = enUSPosixLocale
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        
        let iso8601String = dateFormatter.string(from: Date())
        return iso8601String;
    }
    
    static func isNetworkAvailable() -> Bool{
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }
        
        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        
        return (isReachable && !needsConnection)
    }
    
    func basicEventProperties(type: String, eventObject:Dictionary<String,Any>) -> Dictionary<String,Any>{
        var payload : [String:Any] = ["apikey":Primed.shared.publicKey,
                                      "ts":Int64(Date().timeIntervalSince1970 * 1000),
                                      "sid":self.sid,
                                      "did":self.did,
                                      "source":"APP",
                                      "sdkId":3,
                                      "sdkVersion":"0.0.3",
                                      "type":type.uppercased(),
                                      "eventObject":eventObject
        ]
        
        if(self.customProperties != nil){
            payload["customProperties"] = self.customProperties
        }
        return payload
    }
    
    func emitPayload(payload: Dictionary<String,Any>){
        if(PrimedTracker.isNetworkAvailable() && self.socketConnected) {
            self.socket.emit("event", with: [payload as Any])
        }
        else {
            print("[PRIMED] collector not accessible, writing payload to buffer")
            
            // We allow the savedPayloads buffer to be exactly 1000 entries,
            // if we exceed this number we pop the oldest entry from the buffer
            // prior to adding the new entry
            if savedPayloads.count < savedPayloadsBufferSize {
                self.saveToPlist(payload: payload)
            } else {
                print("[PRIMED] savedPayloads exceeds limit of \(savedPayloadsBufferSize), throwing out oldest entry")
                self.removeFirstInPayloads()
                self.saveToPlist(payload: payload)
            }
        }
    }
    
    @objc func emitFromDatabase() {
        print("[PRIMED] going to emit from database: numPayloads=\(self.savedPayloads.count)")
        while (
            self.savedPayloads.count > 0 &&
            PrimedTracker.isNetworkAvailable() &&
            self.socketConnected
        ) {
            self.emitPayload(payload: self.savedPayloads.firstObject as! Dictionary<String, Any>)
            self.removeFirstInPayloads()
        }
    }
    
    func startHeartbeatTimer (interval:Int) {
        guard self.heartBeatTimer == nil else { return }
        
        // We are restarting the session so we reset the heartbeat to 0
        self.heartBeatIncrement = 0
        
        if #available(iOS 10.0, *) {
            self.heartBeatTimer = Timer.scheduledTimer(
                withTimeInterval: TimeInterval(interval),
                repeats: true,
                block: { (timer) in
                    self.trackHeartbeat()
                }
            )
        } else {
            self.heartBeatTimer = Timer.scheduledTimer(
                timeInterval: TimeInterval(interval),
                target: self,
                selector:#selector(self.trackHeartbeat) ,
                userInfo: nil,
                repeats: true
            )
        }
    }
    
    func startPayloadQueueTimer (interval:Int) {
        guard self.payloadQueueTimer == nil else { return }
        
        if #available(iOS 10.0, *) {
            self.payloadQueueTimer = Timer.scheduledTimer(
                withTimeInterval: TimeInterval(interval),
                repeats: true,
                block: { (timer) in
                    self.emitFromDatabase()
                }
            )
        } else {
            self.payloadQueueTimer = Timer.scheduledTimer(
                timeInterval: TimeInterval(interval),
                target: self,
                selector:#selector(self.emitFromDatabase) ,
                userInfo: nil,
                repeats: true
            )
        }
    }
    
    func stopHeartbeatTimer() {
        self.heartBeatTimer?.invalidate()
        self.heartBeatTimer = nil
    }
    
    func stopPayloadQueueTimer() {
        self.payloadQueueTimer?.invalidate()
        self.payloadQueueTimer = nil
    }
    
    //MARK: INITIALIZATION
    public func initPrimedTracker(publicKey: String, secretKey: String, connectionString: String, trackingConnectionString: String, heartbeatInterval: Int = 10, eventQueueRetryInterval: Int = 600){
        
        if(Primed.shared.publicKey != ""){
            print("[PRIMED] Please use only one initialisation for PrimedTracker")
            return;
        }
        
        Primed.shared.publicKey = publicKey
        Primed.shared.secretKey = secretKey
        Primed.shared.connectionString = connectionString
        
        self.savedPayloads = NSMutableArray.init(contentsOfFile: PrimedTracker.pathPayloads()) ?? NSMutableArray();
        self.primedTrackerAvailable = true;
        
        self.manager = SocketManager(socketURL: URL(string: trackingConnectionString)!, config: [.log(false), .compress, .forceWebsockets(true)])
        self.socket = self.manager.socket(forNamespace: "/v1")
        
//        self.socket.onAny {
//            print("[PRIMED] Got event: \($0.event), with items: \($0.items!)")
//        }
        
        self.socket.on(clientEvent: .connect) {data, ack in
            self.socketConnected = true
            self.emitFromDatabase()
            print("[PRIMED] socket connected")
        }
        
        self.socket.on(clientEvent: .disconnect) {data, ack in
            self.socketConnected = false
            print("[PRIMED] socket disconnected")
        }
        
        self.socket.connect()
        
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: OperationQueue.main) { (note) in
            print("[PRIMED] application to foreground")
            
            // We assign a new session ID (`sid`)
            self.sid = NSUUID().uuidString
            
            self.startHeartbeatTimer(interval: heartbeatInterval)
            self.startPayloadQueueTimer(interval: eventQueueRetryInterval)
            
            self.trackStart(uri: "")
        }

        NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification, object: nil, queue: OperationQueue.main) { (note) in
            print("[PRIMED] application to background")
            
            self.stopHeartbeatTimer()
            self.stopPayloadQueueTimer()
            
            self.trackEnd()
        }
        
        self.startHeartbeatTimer(interval: heartbeatInterval)
        self.startPayloadQueueTimer(interval: eventQueueRetryInterval)
        
        self.trackStart(uri: "")
    }
    
    //MARK: API MAIN FUNCTIONS
    @objc func trackHeartbeat(){
        let customProperties: Dictionary<String,Any> = [:]
        let payload = self.basicEventProperties(type: "heartbeat", eventObject: ["i":self.heartBeatIncrement, "customProperties": customProperties])
        self.emitPayload(payload: payload)
        self.heartBeatIncrement += 1
    }
    
    public func trackClick(x: Int, y:Int, interactionType:InteractionType = .InteractionType_NONE){
        let customProperties: Dictionary<String,Any> = [:]
        let payload = self.basicEventProperties(type: "click", eventObject: ["x":x,
                                                                                 "y":y,
                                                                                 "interactionType": interactionType,
                                                                                 "customProperties": customProperties])
        self.emitPayload(payload: payload)
    }

    public func trackView(uri: String, customProperties: Dictionary<String,Any> = [:]){
        let payload = self.basicEventProperties(type: "view", eventObject: ["uri":uri,
                                                                            "customProperties":customProperties])
        self.emitPayload(payload: payload)
    }
    
    public func trackScroll(scrollDirection: ScrollDirection, distance: Int){
        let payload = self.basicEventProperties(type: "distance", eventObject: ["direction":scrollDirection,
                                                                                "distance":distance])
        self.emitPayload(payload: payload)
    }
    
    public func trackPositionChange(location: CLLocation){
        let payload = self.basicEventProperties(type: "positionChange", eventObject: ["customProperties": [
                                                        "latitude":location.coordinate.latitude,
                                                        "longitude":location.coordinate.longitude,
                                                        "accuracy":Float(location.horizontalAccuracy) + 0.00001]
        ])
        self.emitPayload(payload: payload)
    }
    
    func trackStart(uri: String, customProperties: Dictionary<String,Any> = [:]){
        let payload = self.basicEventProperties(type: "start", eventObject: ["ua":self.getUA(),
                                                                             "now":PrimedTracker.getDate_iso8601(),
                                                                             "uri":uri,
                                                                             "screenWidth":UIScreen.main.bounds.size.width,
                                                                             "screenHeight":UIScreen.main.bounds.size.height,
                                                                             "viewPortWidth":UIScreen.main.bounds.size.width,
                                                                             "viewPortHeight":UIScreen.main.bounds.size.height,
                                                                             "customProperties":customProperties])
        self.emitPayload(payload: payload)
    }
    
    func trackEnd(){
        let payload = self.basicEventProperties(type: "end", eventObject: [:])
        self.emitPayload(payload: payload)
    }
    
    public func trackCustomEvent(eventType: String, customProperties: Dictionary<String,Any>){
        var evtObj = customProperties
        if(customProperties["customProperties"] == nil){
            evtObj["customProperties"] = [:]
        }
        let payload = self.basicEventProperties(type: eventType, eventObject: evtObj)
        self.emitPayload(payload: payload)
    }
    
    public func trackPersonalise(data: Dictionary<String,Any>){
        var evtObj = data
        if(data["customProperties"] == nil){
            evtObj["customProperties"] = [:]
        }
        let payload = self.basicEventProperties(type: "personalise", eventObject: evtObj)
        self.emitPayload(payload: payload)
    }
    
    public func trackConvert(data: Dictionary<String,Any>){
        let payload = self.basicEventProperties(type: "convert", eventObject: data)
        self.emitPayload(payload: payload)
    }
    
    //MARK: GETTERS
    public func getDID() -> String{
        return self.did;
    }
    public func getSID() -> String{
        return self.sid;
    }
}
