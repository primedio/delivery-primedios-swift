//
//  Primed.swift
//  Pods
//
//  Created by PrimedIO on 8/29/18.
//

import UIKit
import CryptoSwift
import Alamofire

public typealias APISuccess = (Dictionary<String,Any>) -> ()
public typealias APIFailed = (String) -> ()
public typealias APIHealth = (String) -> ()
public typealias APIResult = (ResultSet) -> ()

public class Primed {
    
    public static let shared = Primed()
    
    var publicKey: String = ""
    var secretKey: String = ""
    var connectionString: String = ""
    
    init() {
        
    }
    
    private func api_headers() -> Dictionary<String,String> {
        let nonce = String(Int64(Date().timeIntervalSince1970))
        let sha512Signature = (self.publicKey + self.secretKey + nonce).sha512()
        
        let headers: HTTPHeaders = [
            "Content-Type":"application/json",
            "X-Authorization-Key":Primed.shared.publicKey,
            "X-Authorization-Signature":sha512Signature,
            "X-Authorization-Nonce":nonce
        ]
        return headers
    }
    
    public func initPrimed(publicKey: String, secretKey: String, connectionString: String){
        
        if(Primed.shared.publicKey != ""){
            print("[PRIMED] Please use only one initialisation for Primed");
            return;
        }
        
        Primed.shared.publicKey = publicKey
        Primed.shared.secretKey = secretKey
        Primed.shared.connectionString = connectionString
    }
    
    public func personalise(campaign: String, signals: Dictionary<String, Any> = [:], limit: Int = 10, abvariantLabel: String? = nil, success: @escaping APIResult, failed: @escaping APIFailed){
        
        var trackSignals : Dictionary<String, Any> = signals
        if(!signals.keys.contains("did")){
            if(PrimedTracker.sharedTracker.primedTrackerAvailable){
                trackSignals["did"] = PrimedTracker.sharedTracker.getDID()
            }
        }
        if(!signals.keys.contains("sid")){
            if(PrimedTracker.sharedTracker.primedTrackerAvailable){
                trackSignals["sid"] = PrimedTracker.sharedTracker.getSID()
            }
        }
        
        let jsonData = try! JSONSerialization.data(withJSONObject: trackSignals, options: .prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        
        let parameters: Parameters
        if let abvariantLabel = abvariantLabel {
            parameters = [
                "campaign":campaign,
                "limit":limit,
                "abvariant":abvariantLabel,
                "signals":jsonString,
            ]
        } else {
            parameters = [
                 "campaign":campaign,
                 "limit":limit,
                 "signals":jsonString,
            ]
        }
        
        Alamofire.request(Primed.shared.connectionString + "/api/v1/personalise", method: .get, parameters: parameters, headers: self.api_headers()).validate().responseJSON { response in

            if(response.result.isSuccess){
                let jsonRespond: Dictionary = response.result.value as! [String: Any]
                
                print(PrimedTracker.sharedTracker.primedTrackerAvailable)
                
                if(PrimedTracker.sharedTracker.primedTrackerAvailable){
                    PrimedTracker.sharedTracker.trackPersonalise(data: ["guuid":jsonRespond["guuid"]!])
                }
                var resultArray : Array<Result> = []
                
                for element in jsonRespond["results"] as! NSArray {
                    let tmpElement = element as! Dictionary<String, Any>
                    let target = tmpElement["target"] as! Dictionary<String,Any>
                    let value = target["value"] as! Dictionary<String,Any>
                    
                    let result = Result(ruuid: tmpElement["ruuid"] as! String, value: value)
                    resultArray.append(result)
                }
                let resultSet = ResultSet(guuid: jsonRespond["guuid"] as! String, results: resultArray)

                success(resultSet)
            }
            else{
                failed(response.debugDescription)
            }
        }
    }
    
    public func convert(ruuid: String, data: Dictionary<String, Any> = [:]){
        Alamofire.request(Primed.shared.connectionString + "/api/v1/conversion/" + ruuid, method: .post, parameters: data, headers: self.api_headers()).validate().responseJSON { response in
            
            if(response.result.isSuccess){
                if(PrimedTracker.sharedTracker.primedTrackerAvailable){
                    let data: Dictionary<String,Any> = ["customProperties": ["ruuid": ruuid]]
                    
                    PrimedTracker.sharedTracker.trackConvert(data: data)
                }
            }
        }
    }
    
    public func health(success: @escaping APIHealth, failed: @escaping APIHealth){
        Alamofire.request(Primed.shared.connectionString + "/api/v1/health", method: .get, headers: self.api_headers()).validate(statusCode: 200..<300).responseData { response in
            
            switch response.result {
            case .success:
                success("HealthGood")
            case .failure(let error):
                failed(error.localizedDescription)
            }
        }
    }
    
}
