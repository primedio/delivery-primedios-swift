# Installation with CocoaPods

[CocoaPods](http://cocoapods.org) is a dependency manager for iOS, which automates and simplifies the process of using 3rd-party libraries. You can install it with the following command:

```bash
$ gem install cocoapods
```

# Installation

Add PrimedSwift pod to `Podfile`

```ruby
pod 'PrimedSwift', '~> <VERSION>'
```

Then, run the following command:

```ruby
pod install
```

### Import pod

```swift
import PrimedSwift
```

# Usage Primed calls

## Initialisation

```swift
Primed.shared.initPrimed(publicKey: "mypubkey", secretKey: "mysecretkey", connectionString: "API_URL_GO_HERE")
```

## Personalise (ASYNC):

This call obtains predictions for a given campaign and calls the callback function with the result. Personalisation requires at least a campaign key (NB, this is not the campaign name), e.g. `frontpage.recommendations`.

```swift
let dictSignals : [String:String] = ["device":"iphone","userid":"someuserid"]

Primed.shared.personalise(campaign: "frontpage.article.bottom", signals: dictSignals, limit: 3, abvariantLabel: "A", success: { (response) in
    //Handle response
    let output : NSMutableString = ""
    output.append("guuid:" + response.guuid)
    for result in response.results{
        output.append("ruuid: " + result.ruuid) // access the `ruuid` property
        output.append("value: \(result.value)") // access the `value` property
    }
    self.txtContsole.text = String(output)
}) { (message) in
    //Handle message
    print(message)
}
```

| arg                               | type                             | required                              | description                                                                           | example                           |
| --------------------------------- | -------------------------------- | ------------------------------------- | ------------------------------------------------------------------------------------- | --------------------------------- |
| campaign                          | String                           | Yes                                   | campaign key for which personalisation is retrieved                                   | `frontpage.recommendations`       |
| signals                           | Dictionary<String \*, String \*> | No (defaults to `[:]`)                | key, value pairs of signals (itemId currently being viewed, deviceId, location, etc.) | `{itemId: '1234', userId: 'xyz'}` |
| limit                             | Int                              | No (defaults to `5`)                  | number of desired results                                                             | `10`                              |
| abvariantLabel                    | String                           | No (defaults to `WRANDOM` assignment) | specify A/B variant for which to retrieve personalisation                             | `A`                               |
| success:\^(Dictionary \*response) | _callback function_              | Yes                                   | called when results returned succesfully                                              |                                   |
| failed:\^(String \*message)       | _callback function_              | Yes                                   | called in case of failure                                                             |                                   |

## Convert (ASYNC):

Upon successful personalisation, a list of results will be returned. Each result will contain a variable payload: the idea here is that PrimedIO is generic and supports freeform `Targets`, which can be item ID's (usually used in lookups after the personalise call), URL's, Boolean values, and any combination of these. Additionally, each result will contain a unique RUUID (Result UUID), randomly generated for this particular call and `Target`. It is used to track the conversion of this particular outcome, which is used to identify which of the A/B variants performed best. Conversion is also generic, but generally we can associate this with clicking a particular outcome. In order for PrimedIO to register this feedback, another call needs to be made upon conversion. This in turn allows the system to evaluate the performance (as CTR) of the underlying Model (or blend).

```swift
Primed.shared.convert(ruuid: "RUUID_GO_HERE")

let dictData : [String:Any] = ["device":"iphone","userid":"someuserid"]
Primed.shared.convert(ruuid: "RUUID_GO_HERE", data: dictData)
```

| arg   | type                          | required | description                            | example                                  |
| ----- | ----------------------------- | -------- | -------------------------------------- | ---------------------------------------- |
| ruuid | String                        | Yes      | ruuid for which to register conversion | `"6d2e36d1-1b58-4fbc-bea8-868e3ec11c87"` |
| data  | Dictionary<String \*, Any \*> | No       | freeform data payload                  | `{ heartbeat: 0.1 }`                     |

# Usage Primed Tracker

## Initialisation

```swift
PrimedTracker.sharedTracker.initPrimedTracker(publicKey: "mypubkey", secretKey: "mysecretkey", connectionString: "API_URL_GO_HERE", trackingConnectionString: "WEBSOCKET_URL_GO_HERE")
```

**customBasicProperties**
Add custom basic properties.

```swift
PrimedTracker.sharedTracker.customProperties = ["name":"Joe","age":"32"]
```

## Track events

**trackClick**
Sends a click event to Primed.

```swift
func trackClick(x: Int, y:Int, interactionType:InteractionType = .InteractionType_NONE)
```

**trackView**
Sends a view event to Primed. The event requires at least a unique identifier (`uri`) for the page, or view that the user is viewing. The `uri` can be a typical web url (e.g. `http://example.com/articles/1`), or it can indicate a hierarchical view identifer (e.g. `app.views.settings`). Additionally, the call expects a `customProperties` `Dictionary`, which holds user-defined key-value properties. The keys are always `String` and values can be any (boxed) primitive type (`Int`, `Float`, `String`, etc.).

```swift
func trackView(uri: String, customProperties: Dictionary<String,Any> = [:])
```

**trackScroll**
Sends a scroll event to Primed. The event requires a `ScrollDirection` argument, which indicates whether the user scrolled up, down, left or right and a `distance` in pixels.

```swift
func trackScroll(scrollDirection: ScrollDirection, distance: Int)
```

**trackEnterViewport**
Sends an enterViewPort event to Primed. This event is generally called whenever an items appears into view for the user. the call expects a `customProperties` Dictionary, which holds user-defined key-value properties. The keys are always `String` and values can be any (boxed) primitive type (`Int`, `Float`, `String`, etc.).

```swift
func trackEnterViewPort(customProperties: Dictionary<String,Any>)
```

**trackExitViewport**
Sends an exitViewPort event to Primed. This event is generally called whenever an items disappears from view for the user. the call expects a `customProperties` Dictionary, which holds user-defined key-value properties. The keys are always `String` and values can be any (boxed) primitive type (`Integer`, `Float`, `String`, etc.).

```swift
func trackExitViewPort(customProperties: Dictionary<String,Any>)
```

**trackPositionChange**
Sends a positionChange event to Primed.

```swift
func trackPositionChange(location: CLLocation)
```

**trackStart**
Sends a start event to Primed. The event requires at least a unique identifier (`uri`) for the page, or view that the user entered the application on (usually the start or homepage). The `uri` can be a typical web url (e.g. `http://example.com/articles/1`), or it can indicate a hierarchical view identifer (e.g. `app.views.settings`). Additionally, the call expects a `customProperties` Dictionary, which holds user-defined key-value properties. The keys are always `String` and values can be any (boxed) primitive type (`Int`, `Float`, `String`, etc.).

```swift
func trackStart(uri: String, customProperties: Dictionary<String,Any> = [:])
```

**trackEnd**
Sends a end event to Primed. The event expects no arguments.

```swift
func trackEnd()
```

**trackCustomEvent**
User defined event. For example defining a custom `VIDEOSTART` event, which takes one custom property (itemId), looks as follows:

```swift
PrimedTracker.sharedTracker.trackCustomEvent(eventType: "VIDEOSTART", customProperties: ["itemId":"32"])
```

```swift
func trackCustomEvent(eventType: String, customProperties: Dictionary<String,Any>)
```
